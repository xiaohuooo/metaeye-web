const pages = [
    {
        "title" : 'Getting Started',
        "subPages" : [
            {
                "title" : '1.1 Welcome Message',
                "link" : '1.1.md'
            },
            {
                "title" : '1.2 How to create a MetaEye account',
                "link" : '1.2.md'
            },
            {
                "title" : '1.3 What is a cryptocurrency wallet and why do you need one?',
                "link" : '1.3.md'
            },
            {
                "title" : '1.4 How to enter the MetaEye metaverse',
                "link" : '1.4.md'
            }
        ]
    },
    {
        "title" : 'Eye',
        "subPages" : [
            {
                "title" : '2.1 What is EYE',
                "link" : '2.1.md'
            },
            {
                "title" : '2.2 What is the use of EYE?',
                "link" : '2.2.md'
            },
            {
                "title" : '2.3 Buy and sell EYE',
                "link" : '2.3.md'
            },
            {
                "title" : '2.4 About more EYE',
                "link" : '2.4.md'
            },
        ]
    },
    {
        "title" : 'LAND',
        "subPages" : [
            {
                "title" : '3.1 What is land?',
                "link" : '3.1.md'
            },
            {
                "title" : '3.2 What can I do with the land?',
                "link" : '3.2.md'
            },
            {
                "title" : '3.3 MetaEye Land Sales Roadmap',
                "link" : '3.3.md'
            },
            {
                "title" : '3.4 If buying/selling land on a third party platform?',
                "link" : '3.4.md'
            },
            {
                "title" : '3.5 Frequently asked questions about buying, selling and leasing land',
                "link" : '3.5.md'
            },
            {
                "title" : '3.6 More notes on land',
                "link" : '3.6.md'
            },
        ]
    },
    {
        "title" : 'DNS',
        "subPages" : [
            {
                "title" : '4.1 What is DNS',
                "link" : '4.1.md'
            },
            {
                "title" : '4.2 How to get DNS',
                "link" : '4.2.md'
            },
            {
                "title" : '4.3 How to add DNS to LAND',
                "link" : '4.3.md'
            }
        ]
    },
    {
        "title" : 'Stake',
        "subPages" : [
            {
                "title" : '5.1 About Staking',
                "link" : '5.1.md'
            },
            {
                "title" : '5.2 Staking Dashboard',
                "link" : '5.2.md'
            },
            {
                "title" : '5.3 LAND Staking Mining',
                "link" : '5.3.md'
            },
            {
                "title" : '5.4 Liquidity Staking Mining',
                "link" : '5.4.md'
            },
            {
                "title" : '5.5 How to withdraw',
                "link" : '5.5.md'
            },
            {
                "title" : '5.6 General common staking issues',
                "link" : '5.6.md'
            }
        ]
    },
    {
        "title" : 'NFT Assets',
        "subPages" : [
            {
                "title" : '6.1 What is NFT? (NFT\'s ERC-721 and the popularity of ownership)',
                "link" : '6.1.md'
            },
            {
                "title" : '6.2 NFT Mint (space minting NFT functions)',
                "link" : '6.2.md'
            },
            {
                "title" : '6.3 NFT Asset Trading',
                "link" : '6.3.md'
            }
        ]
    },
    {
        "title" : 'Virtual Space',
        "subPages" : [
            {
                "title" : '7.1 Design and build space',
                "link" : '7.1.md'
            },
            {
                "title" : '7.2 Virtual Space Applications',
                "link" : '7.2.md'
            },
            {
                "title" : '7.3 Editor',
                "link" : '7.3.md'
            },
            {
                "title" : '7.4 Frequently asked questions about space',
                "link" : '7.4.md'
            }
        ]
    },
    {
        "title" : 'Marketplace',
        "subPages" : [
            {
                "title" : '8.1 MetaEye Rental Marketplace',
                "link" : '8.1.md'
            },
            {
                "title" : '8.2 MetaEye Secondary Market',
                "link" : '8.2.md'
            },
            {
                "title" : '8.3 How to rent/lease space from MetaEye Marketplace',
                "link" : '8.3.md'
            },
            {
                "title" : '8.4 General common market issues',
                "link" : '8.4.md'
            }
        ]
    },
    {
        "title" : 'Marketing System',
        "subPages" : [
            {
                "title" : '9.1 MetaEye Immersion Marketing',
                "link" : '9.1.md'
            },
            {
                "title" : '9.2 MetaEye Meta-Universe Marketing',
                "link" : '9.2.md'
            }
        ]
    },
    {
        "title" : 'Accounts and Wallets',
        "subPages" : [
            {
                "title" : '10.1 Frequently Asked Questions',
                "link" : '10.1.md'
            },
            {
                "title" : '10.2 Transaction Verification',
                "link" : '10.2.md'
            },
            {
                "title" : '10.3 Withdraw funds from your MetaEye account',
                "link" : '10.3.md'
            },
            {
                "title" : '10.4 Add EYE & LAND tokens to your wallet',
                "link" : '10.4.md'
            },
            {
                "title" : '10.5 Wallet Security',
                "link" : '10.5.md'
            }
        ]
    },
    {
        "title" : 'Other',
        "subPages" : [
            {
                "title" : '11.1 About MetaEye',
                "link" : '11.1.md'
            },
            {
                "title" : '11.2 About MetaEye Team',
                "link" : '11.2.md'
            },
            {
                "title" : '11.3 MetaEye White Paper',
                "link" : '11.3.md'
            },
            {
                "title" : '11.4 MetaEye Roadmap',
                "link" : '11.4.md'
            },
            {
                "title" : '11.5 What is the MetaEye Foundation?',
                "link" : '11.5.md'
            },
            {
                "title" : '11.6 What is NFT?',
                "link" : '11.6.md'
            },
            {
                "title" : '11.7 Community Chat and Support',
                "link" : '11.7.md'
            }
        ]
    },
];

export default pages;