
# **MetaEye - Other**
Feel free to use MetaEye's official documentation and accessibles.

***

#### **About MetaEye**

<br>

+ **When was MetaEye founded?**

The company was co-founded by Lux and Badre Bedro in 2017. We are a metaverse eco-platform focused on marketing and innovative thinking, aiming to make it easy for every user to experience and own their own metaverse world.

<p align="center">
    <img decoding="async" src="/white-paper-img/p11-1-logo.png" alt="logo">
</p>

+ **Where are MetaEye's offices and headquarters located?**

MetaEye is headquartered in the UK and our teams are located around the world, with offices and staff who can get things done in the most efficient way, no matter where they are, MetaEye is an international and diverse business that brings together the most advanced blockchain technology team talent, knowledge and skills in the world, and we aim to build a metaverse for all of humanity.

+ **How can MetaEye's ecology be fully decentralized?**

MetaEye will be decentralized in the next two years with the user as the core, implementing decentralized autonomy (DAO), LAND will be 100% owned by users, and LAND holders can have the voting right of DAO governance, which is a bottom-up decision making mechanism, simply put, to own LAND means to become a shareholder of MetaEye ecology, and for the development of MetaEye and major decisions, shareholders can vote to decide the direction of the ecology. For the development of MetaEye and major decisions, shareholders can vote to decide the direction of the ecology, and gradually the control of MetaEye ecology will be handed over from the platform to the users who own LAND.

+ **What is MetaEye's growth forecast?**

Within two to three years of launch, MetaEye will have at least 2 million brands, businesses or artists on board, and within five years, we aim to grow by 50 million global users



There will be more opportunities in the future to further give you the chance to prove your trust and earn even more rewards.

