# **MetaEye**

***

#### **MetaEye White Paper**

<br>

This white paper explains in detail the economic model behind the MetaEye project and provides a rigorous discussion of the ecosystem and economic approach to building MetaEye. the MetaEye project is constantly evolving and growing, and some of the approaches outlined in the white paper will be updated and iterated.
See **[MetaEye White Paper]**.
