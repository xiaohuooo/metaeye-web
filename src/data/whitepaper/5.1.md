
# **MetaEye - Staking**
Feel free to use MetaEye's official documentation and accessibles.
***

#### **About Staking**

<br>

LAND is a utility and governance token used throughout the MetaEye meta-universe. To maintain a robust ecosystem, the MetaEye metaverse uses a proof-of-stake consensus mechanism. Under this system, network participants who want to support the blockchain by verifying new transactions and adding new blocks must "stake" a certain amount of LAND, staking to lock your assets in order to participate and help maintain the security of the network's blockchain. In exchange for locking your assets and participating in the network verification, the verifier is rewarded with an EYE token in the form of this cryptocurrency, called the staking reward.
In addition the MetaEye Meta-Universe is built on user participation and we want to encourage the long-term mutual development of our users and the ecosystem. Staking LAND tokens supports this vision and builds bridges between all LANDs in the ecosystem.

<p align="center">
    <img decoding="async" src="/white-paper-img/p5-1-strike.png" alt="logo">
</p>

There will be more opportunities in the future to further give you the chance to prove your trust and earn even more rewards.

