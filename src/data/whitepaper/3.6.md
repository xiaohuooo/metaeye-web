# **MetaEye**

***

#### **More notes on LAND**

<br>

+ **How many LANDs are there in MetaEye?**

Total 121,776 LAND

+ **Where can I see the land?**

On the map on the MetaEye website. Click on "Map" on the left toolbar and enter the coordinates to search for your LAND

+ **How much buildable space is available on LAND?**

Buy a LAND to build up to 20 space properties

+ **Is there a limit to how many LANDs I can have?**

No, there is no limit to the number of LANDs you can have

+ **Can I connect my LANDs together to form larger plots?**

Yes, as long as your LANDs are adjacent to each other, you can see how they can be built into a shopping district that can accommodate more tenants

+ **When I pass through the portal of space, will I be transported to other lands?**

Yes, you will have the possibility to teleport to other lands, provided that the LAND in your space has staked its interest

+ **How do I add LAND tokens to my MetaMask wallet?**

Please see **[How to add EYE tokens to my MetaMask wallet]**.