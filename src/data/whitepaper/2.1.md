
# **MetaEye - EYE**
Feel free to use MetaEye's official documentation and accessibles.

***

#### **What is EYE?**

<br>

EYE is an ERC-20 token built on the ethereum blockchain. It is the primary utility token and currency of the MetaEye metaverse ecosystem. It is also a governance token.

<p align="center">
    <img decoding="async" src="/white-paper-img/p2-1-logo.png" alt="logo">
</p>

Therefore, buying and selling irreplaceable tokens (NFTs), such as LAND and DNS, from MetaEye's marketplace will cost a certain amount of EYE, as well as the space level of the land will require EYE consumption for construction

You also need to hold an EYE to participate in the governance of the MetaEye metaverse.