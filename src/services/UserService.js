const ACCOUNT_CKID='account';
const ACCOUNT_ID_CKID='account-id';
const USER_CKID='login-user';
const USER_NAME_CKID='login-user-name';
const USER_PSD_CKID='login-user-psd';

class UserService {
    async conectWallet() {
        if (typeof window.ethereum === 'undefined') {
            console.error('MetaMask is not installed!');
            return '';
        }

        try {
            const permissions = await window.ethereum.request({
                method: "wallet_requestPermissions",
                params: [
                    {
                        eth_accounts: {}
                    }
                ]
            });

            return permissions[0].caveats[0].value[0].toString();
        } catch (e) {
            console.error(`Failed to connect metamask.`);
            return '';
        }
    }

    loginUser(u, n, p) {
        localStorage.setItem(USER_CKID, u);
        localStorage.setItem(USER_NAME_CKID, n);
        localStorage.setItem(USER_PSD_CKID, p);
        localStorage.setItem(ACCOUNT_ID_CKID, getRandomID());
    }

    logoutUser() {
        localStorage.removeItem(USER_CKID);
        localStorage.removeItem(USER_NAME_CKID);
        localStorage.removeItem(USER_PSD_CKID);
        localStorage.removeItem(ACCOUNT_ID_CKID);
    }

    getUserDetails() {
        return {
            name: localStorage.getItem(USER_NAME_CKID),
            user: localStorage.getItem(USER_CKID),
            id: localStorage.getItem(ACCOUNT_ID_CKID),
        };
    }

    connectWalletAccount(wallet) {
        if(wallet === '') {
            this.removeWalletAccount();
            return;
        }
        localStorage.setItem(ACCOUNT_CKID, wallet);
    }

    removeWalletAccount() {
        localStorage.removeItem(ACCOUNT_CKID);
    }

    getWalletAccount() {
        return localStorage.getItem(ACCOUNT_CKID);
    }

    getAccountID() {
        return localStorage.getItem(ACCOUNT_ID_CKID);
    }

    needLogin() {
        const user = this.getUserDetails().user;
        return user === undefined || user === null || user === '';
    }

    isValidEmail(email) {
        const regex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
        return regex.test(email);
    }
}

function getRandomID() {
    let id = '';
    for (let i = 0; i < 8; i++) {
        id += Math.floor(Math.random() * 9).toString();
    }

    return id;
}

const userService = new UserService();

export default userService;