import bgMusic from '../assets/musics/as-it-was.mp3';

const audioInstance = new Audio(bgMusic);
audioInstance.loop = true;
audioInstance.volume = 0.1;
audioInstance.muted = true;

// Background music service, audio instance was created here to prevent
// user creating other instance
// ...
class MusicService {
    play() {
        audioInstance.muted = false;
        audioInstance.play().then(r => { });
    }

    stop() {
        audioInstance.muted = true;
        audioInstance.pause();
    }
}

const musicService = new MusicService();

export default musicService;