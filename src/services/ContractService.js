import NFTTokenContract from '../contracts/NFTToken.json';
import NFTTokenSaleContract from '../contracts/NFTTokenSale.json';
import ETHTokenContract from '../contracts/ERC20.json';

import {ethers} from "ethers";
import env from "react-dotenv";

class ContractService {
    async getNFTToken() {
        const provider = await new ethers.providers.Web3Provider(window.ethereum);
        return new ethers.Contract(env.TOKEN_CONTRACT_ADDRESS, NFTTokenContract.abi, provider.getSigner());
    }

    async getNFTTokenSale() {
        const provider = await new ethers.providers.Web3Provider(window.ethereum);
        return new ethers.Contract(env.TOKEN_SALE_CONTRACT_ADDRESS, NFTTokenSaleContract.abi, provider.getSigner());
    }

    async getETHToken() {
        const provider = await new ethers.providers.Web3Provider(window.ethereum);
        return new ethers.Contract(env.ETH_TOKEN_CONTRACT_ADDRESS, ETHTokenContract.abi, provider.getSigner());
    }
}

const contractService = new ContractService();

export default contractService;