import './NotifyWindow.css';
import PopWindow from "components/pop-window/PopWindow";
import NormalButton from "components/buttons/NormalButton";

export default function NotifyWindow(props) {
    return(
        <PopWindow title={props.title} show={props.show}>
            <div className={'default-top-magin'} />
            <div className={'text-normal center-text'}>
                {props.content}
            </div>
            <div className={'default-top-magin'} style={{height: 20}}/>
            <NormalButton content={'Ok'} onClick={props.onConfirm}/>
        </PopWindow>
    )
}