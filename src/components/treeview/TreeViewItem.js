import './TreeView.css'
import React from "react";

export default function TreeViewItem(props) {
    return (
        <div className={'treeview-item-container'}>
            <button className={'item-button'} onClick={props.onClick}>
                <div className={'item-text'}>{props.title}</div>
            </button>
        </div>
    );
}