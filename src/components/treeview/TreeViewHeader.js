import './TreeView.css'
import React, {useState} from 'react';
import AnimateHeight from 'react-animate-height';
import TreeViewItem from "./TreeViewItem";

export default function TreeViewHeader(props) {
    const [expand, setExpand] = useState(true);

    function onExpandClick() {
        setExpand(!expand);
    }

    function isExpand() {
        return expand;
    }

    return (
        <div className={'treeview-header-container'}>
            <div className={'header-content'}>
                <button className={'header-button' + (isExpand() ? ' header-button-hover' : '')}
                        onClick={onExpandClick}>{props.title}</button>
            </div>
            <AnimateHeight duration={500} height={isExpand() ? 'auto' : 0}>
                <div className={'header-children'}>
                    {
                        Object.values(props.subPages).map(item => {
                            return <TreeViewItem key={item.title} id={item.title} title={item.title.substring(item.title.indexOf(' '), item.title.length)} onClick={() => {props.onLink(item.link, item.title)}}/>;
                        })
                    }
                </div>
            </AnimateHeight>

        </div>
    );
}