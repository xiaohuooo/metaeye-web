import 'components/navbars/DefaultNavBar.css';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { isMobile } from 'react-device-detect';

import logo_small_dark from 'assets/icons/logo_small-dark.png'
import logo_small from 'assets/icons/logo_small.png'

export default function DefaultNavBar(props) {
    const logoIcon = props.dark ? logo_small : logo_small_dark;
    return (
        <Navbar collapseOnSelect expand="xl" variant='dark' style={{backgroundColor: (props.dark ? '#23262f' :'rgba(255,255,255,0)')}}>
            <div className={'nav-container' + (isMobile ? ' nav-container-mobile' : '')}>
                <Navbar.Brand href="/">
                    <img src={logoIcon} height={30} width={132} alt={"meta-logo"}/>
                </Navbar.Brand>
            </div>
        </Navbar>
    )
}