import './CheckBox.css'

export default function CheckBox(props) {
    return(
        <label className="check-box-container">
            <input type="checkbox" />
            <span className="check-box-slider"></span>
        </label>
    )
}