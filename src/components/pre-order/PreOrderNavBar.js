import 'bootstrap/dist/css/bootstrap.css';
import './PreOrderNavBar.css'

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import React from 'react';

import logo_small from '../../assets/icons/logo_small.png'
import logo_whitepaper from '../../assets/icons/white-paper-wire.png'
import logo_user from '../../assets/icons/user-logo.png'
import {isMobile } from 'react-device-detect';

import UserService from "../../services/UserService";

export default function PreOrderNavBar(props) {

    const mobileLinkStyle = {
        marginRight: 10,
        flexDirection: "row-reverse",
        display: "flex"
    }

    const browserLinkStyle = {
        marginRight: 50
    }

    const mobileContainerStyle = {
        marginTop: 20,
        marginLeft: 50,
        marginRight: 40
    }

    const browserContainerStyle = {
        marginTop: 20
    }

    return (
        <Navbar collapseOnSelect expand="xl" bg="none" variant="dark">
            <Container fluid style={isMobile ? mobileContainerStyle : browserContainerStyle}>
                {
                    isMobile ? null : <div className={'spacer'} />
                }
                <Navbar.Brand href="/">
                    <img src={logo_small} height={30} width={132} alt={"meta-logo"}/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto"></Nav>
                    <Nav>
                        {
                            isMobile ?
                                <div style={{flexDirection: "column", display: "flex", marginTop: 20}}>
                                    <Nav.Link onClick={props.onLogin} style={mobileLinkStyle}>
                                        { props.loginStatus ? UserService.getUserDetails().name : 'Login' }
                                        <img className={'text-logo'} src={logo_user} height={26} width={26}
                                             alt={"meta-logo"}/>
                                    </Nav.Link>
                                    <Nav.Link href="/white-paper" style={mobileLinkStyle}>
                                        Docs & Resources
                                        <img className={'text-logo'} src={logo_whitepaper} height={26} width={26}
                                                              alt={"meta-logo"}/>
                                    </Nav.Link>

                                </div>
                                :
                                <div style={{display: "flex", flexDirection: "row"}}>
                                    <Nav.Link href="/white-paper" style={browserLinkStyle}>
                                        <img className={'text-logo'} src={logo_whitepaper} height={26} width={26}
                                             alt={"meta-logo"}/> Docs & Resources
                                    </Nav.Link>
                                    <Nav.Link onClick={props.onLogin}>
                                        <img className={'text-logo'} src={logo_user} height={26} width={26}
                                             alt={"meta-logo"}/> { props.loginStatus ? UserService.getUserDetails().name : 'Login' }
                                    </Nav.Link>
                                </div>
                        }
                    </Nav>
                </Navbar.Collapse>
                {
                    isMobile ? null : <div className={'spacer'} />
                }
            </Container>
        </Navbar>
    )
}