import './CountDown.css'
import {useTimer} from "react-timer-hook";
import React from "react";

import CountDownNumber from "./CountDownNumber";

export default function PreOderCountDown(props) {
    const unlockTimespan = new Date("2023-07-1");
    const timeSeconds = (unlockTimespan.getTime() - Date.now()) / 1000;
    const dateStamp = new Date();
    dateStamp.setSeconds(timeSeconds);
    const {
        seconds,
        minutes,
        hours,
        days
    } = useTimer({ expiryTimestamp: dateStamp, onExpire: () => {}, autoStart: true });

    return (
        <div className={'countdown-main'}>
            <CountDownNumber des={'DAYS'} number={days}/>
            <div className={'countdown-colon'}>:</div>
            <CountDownNumber des={'HOURS'} number={hours}/>
            <div className={'countdown-colon'}>:</div>
            <CountDownNumber des={'MINS'} number={minutes}/>
            <div className={'countdown-colon'}>:</div>
            <CountDownNumber des={'SECS'} number={seconds}/>
        </div>
    );
}