import './PreOrderInputField.css'
import HighlightButton from '../buttons/HighlightButton'
import React, {useState} from 'react';

export default function PreOrderInputField(props) {

    const [orderNumber, setOrderNumber] = useState(1);

    function onClick() {
        props.onOrder(orderNumber);
    }

    function onInputChange(event) {
        setOrderNumber(Number(event.target.value));
    }

    return(
        <div className={'input-field-bg'}>
            <input type={'number'} placeholder={props.placeholder} onInput={onInputChange} className={'input-field text-normal'} min="0"/>
            <HighlightButton onClick={onClick} content={'Order'} displayImage={false}/>
        </div>
    );
}