import './PreOderOffcanvas.css'
import '../Styles.css'

import React, {useEffect, useState} from 'react';
import {Offcanvas} from "react-bootstrap";

import defaultAccountLogo from '../../assets/icons/account_default.png'
import transferLogo from '../../assets/icons/transfer-icon.png'
import dmsLogo from '../../assets/icons/prop_logo_dms.png'
import titleLogo from '../../assets/icons/prop_logo_title.png'
import landLogo from '../../assets/icons/prop_logo_land.png'
import tokenLogo from '../../assets/icons/prop_logo_token.png'

import UserService from "../../services/UserService";
import ContractService from "../../services/ContractService";

import HighlightButton from "../buttons/HighlightButton";
import PreOderCountDown from "./PreOderCountDown";
import Const from "utils/Const";
import axios from "axios";

export default function PreOderOffcanvas(props) {
    const [landCount, setLandCount] = useState('Requesting');
    const [accountText, setAccountText] = useState('');
    const [userId, setUserId] = useState('');

    useEffect(() => {
        updateId();
        updateAccountText()
        getBalance().then(r => {
        });
    })

    async function getBalance() {
        const account = UserService.getWalletAccount();
        if (account === null || account === '' || account === undefined) {
            setLandCount('0');
            return;
        }

        const contract = await ContractService.getNFTToken();
        const balance = await contract.balanceOf(account);
        setLandCount(balance.toString());
    }

    async function onConnectWallet() {
        const ac = await UserService.conectWallet();
        UserService.connectWalletAccount(ac);
        updateAccountText();
    }

    function onUserLogout() {
        UserService.logoutUser();
        UserService.removeWalletAccount();
        const rootUrl = Const.serverHttpRoot;
        axios.put(rootUrl + 'user/logout').then(r => {});

        setUserId('')
        setAccountText('');

        props.onLogout();
    }

    function onTransfer() {
        props.onTransfer();
    }

    // function getEmail() {
    //     let userDetails = UserService.getUserDetails();
    //     if (userDetails.user === null || userDetails.user === undefined) {
    //         return 'NOT LOGIN';
    //     }
    //     return userDetails.user;
    // }

    function updateId() {
        let userDetails = UserService.getUserDetails();
        setUserId(userDetails.id);
    }

    function updateAccountText() {
        let account = UserService.getWalletAccount();
        if (account === null || account === undefined) {
            setAccountText('');
        }
        else{
            setAccountText(account.slice(0, 6) + '......' + account.slice(account.length - 4));
        }
    }

    return (
        <Offcanvas placement={'end'} show={props.showStatus} onHide={props.onClose} backdrop="static">
            <Offcanvas.Header closeButton>
                <Offcanvas.Title/>
            </Offcanvas.Header>
            <Offcanvas.Body>
                <div className={'default-top-magin text-title-2'}>
                    Account
                </div>
                <div className={'spliter-dark-3px'}/>
                <div className={'horizontal-flex'}>
                    <img src={defaultAccountLogo} alt={"meta-logo"} height={48} width={48}/>
                    <div className={'vertical-flex'} style={{flex: 1}}>
                        <div className={'horizontal-flex'}>
                            <div className={'horizontal-flex'}>
                                <div className={'account-text text-normal'}> {'ID: ' + userId} </div>
                            </div>
                        </div>
                        <div className={'tiny-top-magin account-text text-normal horizontal-flex'} style={{alignItems:"center"}}>
                            <div style={{marginRight: 10}}> { accountText } </div>
                            <button className={'btn-bind-wallet text-normal'} onClick={onConnectWallet}> {accountText === '' ? 'CONNECT WALLET' : 'SWITCH'} </button>
                        </div>
                    </div>
                    <button className={'btn-logout'} onClick={onUserLogout}/>
                </div>
                <div className={'default-top-magin text-warning'}>
                    {accountText === '' ? 'Connect wallet to display your assets correctly.' : ''}
                </div>
                <div style={{display: "flex", justifyContent: "space-between", marginTop: 100, alignItems: "center"}}>
                    <div className={'form-title text-title-2'}>My Assets</div>
                    <HighlightButton onClick={onTransfer} content={'Transfer'} displayImage={true} imgSrc={transferLogo}
                                     height={'40px'}/>
                </div>
                <div className={'spliter-dark-3px'}/>
                <div className={'property-item default-top-magin '}>
                    <img src={titleLogo} alt={"meta-logo"} height={32} width={32}/>
                    <div className={'property-item-name text-normal'}> Contract</div>
                    {landCount}
                </div>

                <div className={'property-item default-top-magin'}>
                    <img src={landLogo} alt={"meta-logo"} height={32} width={32}/>
                    <div className={'property-item-name text-normal'}> Land</div>
                    {0}
                </div>

                <div className={'property-item default-top-magin'}>
                    <img src={tokenLogo} alt={"meta-logo"} height={32} width={32}/>
                    <div className={'property-item-name text-normal'}> EYE</div>
                    {0}
                </div>

                <div className={'property-item default-top-magin'}>
                    <img src={dmsLogo} alt={"meta-logo"} height={32} width={32}/>
                    <div className={'property-item-name text-normal'}> DMS</div>
                    {0}
                </div>

                <div className={'form-title text-title-2'} style={{marginTop: 100}}>LAND SALE COUNTDOWN</div>
                <div className={'spliter-dark-3px'}/>

                <PreOderCountDown/>
            </Offcanvas.Body>
        </Offcanvas>
    );
}

