import './PreOrderPurscahe.css'
import '../../Styles.css'
import env from "react-dotenv";
import {BigNumber, ethers} from "ethers";
import React, {useEffect, useRef, useState} from "react";
import priceLogo from "../../../assets/icons/price-logo.png";
import contractLogo from "../../../assets/icons/prop_logo_title.png";
import moneyLogo from "../../../assets/icons/money-logo.png";

import UserService from "../../../services/UserService";
import ContractService from "../../../services/ContractService";

import NormalButton from "../../buttons/NormalButton";
import FullscreenLoader from "../../FullscreenLoader";
import PopWindow from  "../../pop-window/PopWindow";
import NotifyWindow from "components/notify-window/NotifiyWindow";

export default function PreOrderPurscahe(props) {
    const [updateCounter, setUndateCounter] = useState(5);
    const [perPrice, setPerPrice] = useState('...')
    const [priceText, setPriceText] = useState('...')
    const [confirmButtonText, setConfirmButtonText] = useState('Approval');
    const [allowanceNeed, setAllowanceNeed] = useState(0);
    const [balanceNeed, setBalanceNeed] = useState(0);
    const [loading, setLoading] = useState(false);
    const intervalObject = useRef();

    const [notifying, setNotifying] = useState({
        title : 'notify',
        content : 'content',
        display: false
    });

    useEffect(() => {
        return () => clearInterval(intervalObject.current);
    });

    useEffect(() => {
        intervalObject.current = setInterval(tick, 1000);
    });

    useEffect(()=>{
        if(props.show) {
            setLoading(false);
            updatePriceAndAllowance().then(_ => {});
        }
    }, [props.show])

    function tick() {
        if (updateCounter <= 0) {
            updatePriceAndAllowance().then(_ => {});
            setUndateCounter(5);
        } else {
            setUndateCounter(updateCounter - 1);
        }
    }

    function onNotifyConfirm() {
        setNotifying({
            display: false
        });
    }

    function closeWindow() {
        props.onBack(false);
    }

    function OrderSuccess() {
        props.onBack(true);
    }

    async function updatePriceAndAllowance() {
        if(!props.show) return;

        // Fetch some data
        const account = UserService.getWalletAccount();
        const ethContract = await ContractService.getETHToken();
        const saleContract = await ContractService.getNFTTokenSale();
        const priceInETH = await saleContract.getNFTRealtimePricePublic(env.TOKEN_CONTRACT_ADDRESS, 0);
        const pticeInUSD = await saleContract.getPrice(env.TOKEN_CONTRACT_ADDRESS);
        const allow = await ethContract.allowance(account, env.TOKEN_SALE_CONTRACT_ADDRESS);
        const balance = await ethContract.balanceOf(account);

        // Do math
        const balanceInEth = parseFloat(ethers.utils.formatEther(balance));
        const ethNormalNumber = parseFloat(ethers.utils.formatEther(priceInETH));
        const allowInEth = parseFloat(ethers.utils.formatEther(allow));
        const totalETH = (ethNormalNumber * props.amount).toFixed(5);
        const priceDisplay = (pticeInUSD / 100).toFixed(2);

        // Set date
        setPerPrice(priceDisplay.toString() + 'USD');
        setPriceText(`${totalETH.toString()} ETH (${(priceDisplay * props.amount).toFixed(2)}USD)`);

        setConfirmButtonText(allowInEth > totalETH ? 'Purscache' : 'Approval');
        setAllowanceNeed(totalETH - allowInEth);
        setBalanceNeed(totalETH - balanceInEth);
    }

    async function onConfirm() {
        if(balanceNeed > 0) {
            setNotifying({
                display: true,
                title: 'Ops',
                content: 'You do not have enough balance to purscache or request approval.'
            })
            return;
        }

        setLoading(true);
        clearInterval(intervalObject.current);

        if(confirmButtonText === 'Approval') {
            try {
                const account = UserService.getWalletAccount();
                const ethContract = await ContractService.getETHToken();
                const balance = await ethContract.balanceOf(account);
                await ethContract.connect(account);
                const tx = await ethContract.approve(env.TOKEN_SALE_CONTRACT_ADDRESS, balance);
                await tx.wait();
            } catch (e) {
                setLoading(false);
                setNotifying({
                    display: true,
                    title: 'Ops',
                    content: e.code
                })
            }
        } else {
            try {
                const account = UserService.getWalletAccount();
                const saleContract = await ContractService.getNFTTokenSale();
                const tx2 = await saleContract.mintPublic(env.TOKEN_CONTRACT_ADDRESS, account, props.amount, 0).then();
                await tx2.wait();
                OrderSuccess();
                return;
            } catch (e) {
                setLoading(false);
                setNotifying({
                    display: true,
                    title: 'Ops',
                    content: e.code
                })
            }
        }

        setLoading(false);
        updatePriceAndAllowance().then(_ => {});
        intervalObject.current = setInterval(tick, 1000);
    }

    return(
        <PopWindow title={'Order'} show={props.show}>
            <div className={'horizontal-flex text-normal'}>
                <div className={'payment-check-item'}>
                    <img src={priceLogo} alt={"meta-logo"} height={48} width={48} className={'payment-icon'}/>
                    Price
                </div>
                <div className={'payment-count-container'}>
                    {perPrice}
                </div>
            </div>
            <div className={'default-top-magin horizontal-flex text-normal'}>
                <div className={'payment-check-item'}>
                    <img src={contractLogo} alt={"meta-logo"} height={48} width={48} className={'payment-icon'}/>
                    Amount
                </div>
                <div className={'payment-count-container'}>
                    x {props.amount}
                </div>
            </div>
            <div className={'default-top-magin horizontal-flex text-normal'}>
                <div className={'payment-check-item'}>
                    <img src={moneyLogo} alt={"meta-logo"} height={48} width={48} className={'payment-icon'}/>
                    In Total
                </div>
                <div className={'payment-count-container'}>
                    {priceText}
                </div>
            </div>
            <div className={'default-top-magin text-normal payment-check-tiptext'}>
                The price will be updated in {updateCounter}s
            </div>

            <div style={{height: 50}} />
            <div className={'payment-bottom-container'}>
                {allowanceNeed > 0 && (
                    <div className={'text-warning'}>
                        Allowance Need: {(allowanceNeed).toFixed(5)} eth
                    </div>
                )}
                {balanceNeed > 0 && (
                    <div className={'text-warning'}>
                        Balance Need: {(balanceNeed).toFixed(5)} eth
                    </div>
                )}
                <div className={'default-top-magin horizontal-flex'} style={{width: '100%', justifyContent: "center"}}>
                    <NormalButton content={'Back'} onClick={closeWindow}/>
                    <div style={{width: '10%'}}></div>
                    <NormalButton content={confirmButtonText} onClick={onConfirm} important={true}/>
                </div>
            </div>
            <NotifyWindow title={notifying.title} show={notifying.display} content={notifying.content} onConfirm={onNotifyConfirm}/>
            <FullscreenLoader show={loading}/>
        </PopWindow>
    )
}