import './PreOrderTransfer.css'
import '../../Styles.css'
import FullscreenLoader from "../../FullscreenLoader";
import PopWindow from  "../../pop-window/PopWindow";
import NormalButton from "../../../components/buttons/NormalButton";
import contractLogo from "../../../assets/icons/prop_logo_title.png";
import userLogo from '../../../assets/icons/hacker-logo.png';
import ContractService from "../../../services/ContractService";
import UserService from "../../../services/UserService";

import React, {useEffect, useState} from "react";

export default function PreOrderTransfer(props) {

    const [number, setNumber] = useState(0);
    const [toUser, setToUser] = useState("");
    const [tipText, setTipText] = useState("");
    const [loading, setLoading] = useState(false);

    useEffect(()=>{
        setTipText('');
    }, [props.show])

    function onBack() {
        props.onBack();
    }

    async function onTransfer() {
        if (number <= 0){
            setTipText('Please enter nft number to transfer.');
            return;
        }

        if (toUser === "") {
            setTipText('Please enter a receiver.');
            return;
        }

        setTipText('');

        try {
            setLoading(true);
            const account = UserService.getWalletAccount();
            const tokenContract = await ContractService.getNFTToken();

            // Check balance
            const balance = await tokenContract.balanceOf(account);
            if(balance < number) {
                setLoading(false);
                setTipText('You dont have enough balance.');
                return;
            }

            const tokenIds = await tokenContract.getTokenIdsOfOwner(account);
            if(tokenIds.length < number) {
                setLoading(false);
                setTipText('You dont have enough balance.');
                return;
            }

            let tokensToTransfer = [];
            for (let i = 0; i < number; i++) {
                tokensToTransfer.push(tokenIds[i]);
            }

            const tx = await tokenContract.safeTransferFromBatch(account, toUser, tokensToTransfer);
            await tx.wait();
            setLoading(false);
        }  catch (e) {
            console.log(e.message)
            setLoading(false);
        }
    }

    function onCountChange(event) {
        setNumber(Number(event.target.value));
    }

    function onAddressChange(event) {
        setToUser(event.target.value);
    }

    return (
        <PopWindow title={'Transfer'} show={props.show}>
            <div className={'horizontal-flex text-normal'}>
                <img src={contractLogo} alt={"meta-logo"} height={48} width={48} className={'payment-icon'}/>
                <input type={'number'} className={'transfer-input-box'} onInput={onCountChange} placeholder={"NUMBER TO TRANSFER"}/>
            </div>
            <div className={'default-top-magin horizontal-flex text-normal'}>
                <img src={userLogo} alt={"meta-logo"} height={48} width={48} className={'payment-icon'}/>
                <input type={'text'} className={'default-top-magin transfer-input-box'} onInput={onAddressChange} placeholder={"To 0x..."} style={{marginBottom: '15px'}}/>
            </div>
            <div className={'default-top-magin text-normal'} style={{flex: 1, color: 'red', alignSelf: "center"}}>
                {
                    tipText
                }
            </div>
            <div className={'default-top-magin horizontal-flex'} style={{width: '100%', justifyContent: "center"}}>
                <NormalButton content={'Back'} onClick={onBack}/>
                <div style={{width: '10%'}}></div>
                <NormalButton content={'Transfer'} onClick={onTransfer} important={true}/>
            </div>
            <FullscreenLoader show={loading}/>
        </PopWindow>
    );
}