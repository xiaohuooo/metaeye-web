import './CountDown.css'

export default function CountDownNumber(props) {

    function getNumberText() {
        if (props.number < 10) return '0' + props.number;
        else return props.number;
    }

    return(
        <div className={'main'}>
            <div className={'box'}>
                <div className={'number-text'}>{getNumberText()}</div>
            </div>
            <div className={'des-text'}>{props.des}</div>
        </div>
    );
}