import './PopWindow.css'
import '../Styles.css'
import React from "react";

export default function PopWindow(props) {
    return(
        props.show ?
            <div className={'overlay-fullscreen center-layout'}>
                <div className={'pop-window-frame center-screen'} style={{width: props.width, height: props.height}}>
                    <div className={'pop-window-title'}>
                        {props.title}
                    </div>
                    <div className={'pop-window-content'}>
                        {props.children}
                    </div>
                </div>
            </div> : null
    );
}