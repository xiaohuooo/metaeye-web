import './FullscreenLoader.css'
import './Styles.css'

export default function FullscreenLoader(props) {
    if (props.show) {
        return (
            <div className={'overlay-fullscreen center-layout'}>
                <div className="fsl-wrapper">
                    <div className="fsl-circle"></div>
                    <div className="fsl-circle"></div>
                    <div className="fsl-circle"></div>
                    <div className="fsl-shadow"></div>
                    <div className="fsl-shadow"></div>
                    <div className="fsl-shadow"></div>
                </div>
            </div>
        )
    } else {
        return null;
    }
}