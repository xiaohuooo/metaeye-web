import './VideoBackground.css'
import React from 'react';

export default class VideoBackground extends React.Component{
    render() {
        return(
            <div className='video-bg-main'>
                <video src={this.props.source} autoPlay loop muted>
                    <div className={'children'}>
                        {this.props.children}
                    </div>
                </video>
            </div>
        );
    }
}
