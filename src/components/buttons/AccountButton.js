import './AccountButton.css'

const logo_wallet = require('../../assets/icons/wallet-wire.png');

export default function AccountButton(props) {

    const getButtonText = () => {
        if (props.connectStatus) return 'Profile'
        else return 'Connect Wallet'
    };

    return(
        <div>
            <img className={'text-logo'} src={logo_wallet} height={26} width={26} alt={"meta-logo"} /> { getButtonText() }
        </div>
    );
}
