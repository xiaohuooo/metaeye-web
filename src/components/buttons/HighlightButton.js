import './HighlightButton.css'

import React from "react";

export default function HighlightButton(props) {
    return (
        <button className={'highlight-button'} onClick={props.onClick} style={{height: props.height}} >
            {
                props.displayImage ?
                    <img src={props.imgSrc} width={props.imgWidth} height={props.imgHeight} alt={'img'} style={{marginRight: '10px'}}/> : null
            }
            {props.content}
        </button>
    );
}