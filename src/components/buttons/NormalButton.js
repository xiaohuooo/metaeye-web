import './NormalButton.css'
import React from "react";

export default function NormalButton(props) {
    return (
        <button className={'normal-button' + (props.important ? ' normal-button-important' : '')}
                onClick={props.onClick}>{props.content}</button>
    );
}