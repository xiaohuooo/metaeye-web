
const development = true;
const Const = {
    serverHttpRoot: development ? 'http://localhost:8080/' : 'https://metaeye.io/api/',
}

export default Const;