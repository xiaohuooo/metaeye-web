import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";

// import PreOrder from './pages/preoder/PreOrder';
import WhitePaper from "./pages/whitepaper/WhitePaper";
import Login from "./pages/user/Login";
import Register from "./pages/user/Register";
import Continue from "./pages/user/continue";
import Index from "pages/index/Index";
import Home from "pages/home/Index";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <Routes>
      {/*<Route path='/' element={<PreOrder />}/>*/}
      <Route path="/" element={<Index />} />
      <Route path="/Home" element={<Home />} />
      <Route path="/user/login" element={<Login />} />
      <Route path="/user/register" element={<Register />} />
      <Route path="/user/continue" element={<Continue />} />
      <Route path="/white-paper" element={<WhitePaper />} />
    </Routes>
  </BrowserRouter>
);
