import './PreOrder.css'
import '../../components/VideoBackground.css'

import React, {useEffect, useState} from 'react';
import PreOrderNavBar from "../../components/pre-order/PreOrderNavBar";
import VideoBackground from "../../components/VideoBackground";
import PreOrderInputField from "../../components/pre-order/PreOrderInputField";
import PreOderOffcanvas from "../../components/pre-order/PreOderOffcanvas";
import PreOrderPurscahe from "../../components/pre-order/payment/PreOrderPurscahe";
import PreOrderTransfer from "../../components/pre-order/transfer/PreOrderTransfer";

import MusicService from "../../services/MusicService";
import UserService from "../../services/UserService";

import videoBg from '../../assets/videos/video-bg.mp4';
import {isMobile} from 'react-device-detect';
import {useNavigate} from "react-router-dom";

export default function PreOrder() {
    let navigate = useNavigate();
    const [offCanvas, setOffCanvas] = useState(false);
    const [displayPurscahe, setDisplayPurscahe] = useState(false)
    const [displayTransfer, setDisplayTransfer] = useState(false)
    const [buyCount, setBuyCount] = useState(0);
    const [login, setLogin] = useState(false);

    function onCloseWalletInfo() {
        setOffCanvas(false);
    }

    function onDisplayWalletInfo() {
        setOffCanvas(true);
    }

    function onLogin() {
        if(login) {
            onDisplayWalletInfo()
        } else {
            navigate('/user/login');
        }
    }

    function onLogout(){
        onCloseWalletInfo()
    }

    function onPurscacheBack(success) {
        setDisplayPurscahe(false);
        if (success) {
            setOffCanvas(true);
        }
    }

    async function onOrderClick(count) {
        const account = UserService.getWalletAccount();
        if (account === undefined || account === null || account === '') {
            const ac = await UserService.conectWallet();
            UserService.connectWalletAccount(ac);
        } else if (count > 0) {
            orderLandNFT(count).then(r => {
            });
        }
    }

    async function orderLandNFT(count) {
        setBuyCount(count);
        setDisplayPurscahe(true);
    }

    function updateLoginStatus() {
        const user = UserService.getUserDetails();
        setLogin(user.user !== null && user.user !== undefined);
    }

    function playBackgroundMusic() {
        MusicService.play();
    }

    function onTransfer() {
        onCloseWalletInfo();
        setDisplayTransfer(true);
    }

    function onTransferBack() {
        setDisplayTransfer(false);
    }

    useEffect(()=> {
        updateLoginStatus();
        // playBackgroundMusic();
    });

    return (
        <div className="facade">
            <VideoBackground source={videoBg}/>
            <div className="overlay-container">
                <div className={'content-container'}>
                    <PreOrderNavBar loginStatus={login} onLogin={onLogin}/>
                    <div className={'title' + (isMobile ? ' title-mobile' : '')}>
                        <h1 className={'title-text'}>
                            Come and explore the future of metaverse though
                        </h1>
                        <h1 className={'title-text'} style={{marginBottom: 60}}>
                            MetaEye As far as the eye can see!
                        </h1>
                        <div className={'input-area'}>
                            <PreOrderInputField label={'Land amount'} placeholder={'Enter Amount'}
                                                onOrder={onOrderClick}/>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <PreOderOffcanvas showStatus={offCanvas}
                                  onLogout={onLogout}
                                  onClose={onCloseWalletInfo}
                                  onTransfer={onTransfer}/>
            </div>
            <PreOrderPurscahe show={displayPurscahe} amount={buyCount} onBack={onPurscacheBack}/>
            <PreOrderTransfer show={displayTransfer} onBack={onTransferBack}/>
        </div>
    );
}


