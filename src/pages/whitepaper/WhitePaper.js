import './WhitePaper.css'
import '../../components/Styles.css'

import ReactMarkdown from "react-markdown";
import rehypeRaw from 'rehype-raw'
import remarkGfm from 'remark-gfm'
import { Scrollbar } from 'react-scrollbars-custom';
import React, {useEffect, useState} from "react";
import { isMobile } from 'react-device-detect';

import DefaultNavBar from "../../components/navbars/DefaultNavBar";
import TreeViewHeader from "../../components/treeview/TreeViewHeader";
import Pages from "data/whitepaper/WhitePaperPages";

export default function WhitePaper(props) {
    const [post, setPost] = useState('');
    const [mobileDisplayCategory, setMobileDisplayCategory] = useState(false);
    const [selection, setSelection] = useState('none');

    useEffect(()=>{
        onLinkPage('1.1.md', 'none');
    }, [])

    function onLinkPage(link, key) {
        setSelection(key);
        setMobileDisplayCategory(false);
        import(`data/whitepaper/${link}`)
            .then(res => {
                fetch(res.default)
                    .then(res => res.text())
                    .then(res => setPost(res))
                    .catch(err => console.log(err));
            }).catch(err => console.log(err));
    }

    function onMobileCategoryClick() {
        setMobileDisplayCategory(!mobileDisplayCategory);
    }

    function renderBrowser() {
        return (
            <div className={'content-facade'}>
                <Scrollbar className={'contentbox treeview-list'}>
                    <div>
                        {
                            Object.values(Pages).map(category => {
                                return <TreeViewHeader id={category.title} key={category.title} title={category.title}
                                                       subPages={category.subPages} onLink={onLinkPage}/>;
                            })
                        }
                    </div>
                </Scrollbar>
                <Scrollbar className={'contentbox content-root'}>
                    <div className={'markdown-box'}>
                        <ReactMarkdown skipHtml={false} children={post} rehypePlugins={[rehypeRaw, remarkGfm]}/>
                    </div>
                </Scrollbar>
            </div>
        );
    }

    function renderMobile() {
        return (
            <div className={'content-facade content-facade-mobile'}>
                <div className={'mobile-category'}>
                    <div className={(mobileDisplayCategory ? 'mobile-category-display' : 'mobile-category-Hidden')}>
                        <Scrollbar className={'contentbox'}>
                            <div>
                                {
                                    Object.values(Pages).map(category => {
                                        return <TreeViewHeader id={category.title} key={category.title} title={category.title}
                                                               subPages={category.subPages} onLink={onLinkPage}/>;
                                    })
                                }
                            </div>
                        </Scrollbar>
                    </div>
                    <div className={'mobile-category-toggle'} onClick={onMobileCategoryClick}>
                        {(mobileDisplayCategory ? '<' : '>') }
                    </div>
                </div>
                {
                    !mobileDisplayCategory ?
                    <Scrollbar className={'contentbox content-root'}>
                        <div className={'markdown-box'}>
                            <ReactMarkdown skipHtml={false} children={post} rehypePlugins={[rehypeRaw, remarkGfm]}/>
                        </div>
                    </Scrollbar> : null
                }

            </div>
        );
    }

    return (
        <div className={'white-paper-facade'}>
            <DefaultNavBar dark={false}/>
            <div className={'tab-container' + (isMobile ? ' tab-container-mobile' : '')}>
                <button className={'tab-button'}>
                    Docs
                </button>
                <input className={'wt-search-bar'} placeholder='Search'/>
            </div>
            {
                isMobile ? renderMobile() : renderBrowser()
            }
        </div>
    );
}