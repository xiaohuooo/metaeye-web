import React from "react";
import logo1 from "assets/素材/向左.png";
export default function title1(props) {
    const { tit1,tit2,tit3,tit4 } = props;
  return (
    <>
      <p style={{ fontSize: "20px" ,color:'#000'}}>{tit1}</p>
      <p style={{ fontSize: "38px", marginTop: "20px",color:'#000' }}>
        {tit2[0]}
        <br />
        {tit2[1]}
      </p>
      <p style={{ fontSize: "28px",marginTop: "20px", color: "#7d7d7d"}}>
      {tit3}
      </p>
      <p style={{ fontSize: "28px", marginTop: "55px", color: "#465968" }}>
        <img
          src={logo1}
          alt=""
          style={{
            transform: "rotateY(180deg)",
            width: "48px",
            verticalAlign: "middle",
          }}
        />
        <span
          style={{
            marginLeft: "10px",
          }}
        >
          {tit4}
        </span>
      </p>
    </>
  );
}
