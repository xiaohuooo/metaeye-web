import "pages/index/center/index.css";
import logo from "assets/素材/指向-左.png";
import Title from "./title";
import Title1 from "./title1";
import React, {useState} from "react";
import logo1 from "assets/素材/向左.png";
import exampleImg01 from "assets/image/index/index-example-img01.png"
import exampleImg02 from "assets/image/index/index-example-img02.png"
import exampleImg03 from "assets/image/index/index-example-img03.png"
import login1 from "assets/image/logo.png"
import bg from "assets/image/1.png"
import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from 'react-bootstrap/Carousel';

const tNav = {
    height: "65px",
    borderBottom: "1px solid gray",
    lineHight: "65px",
    background: "#fff",
    color: "#000",
    lineHeight: "65px",
    paddingLeft: "13%",
};

export default function Index() {
    const [activeIndex, setActiveIndex] = useState(0);
    const handleClick = (index) => {
        setActiveIndex(index);
    };

    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };
    return (
        <div>
            <Carousel>
                <Carousel.Item>
                    <div className="bo1">
                        <div className={'t-box'}>
                            <img src={login1} alt="" />
                            <p style={{fontSize: "32px", margin: '32px 0 21px'}}>开启你的元宇宙之旅</p>
                            <p style={{
                                fontSize: "18px",
                                fontWeight: 'lighter'
                            }}>加入MetaEye社区，体验全新的虚拟世界</p>
                        </div>
                        <div className="line"></div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                <div className="bo1">
                        <div className={'t-box'}>
                            <img src={login1} alt="" />
                            <p style={{fontSize: "32px", margin: '32px 0 21px'}}>开启你的元宇宙之旅</p>
                            <p style={{
                                fontSize: "18px",
                                fontWeight: 'lighter'
                            }}>加入MetaEye社区，体验全新的虚拟世界</p>
                        </div>
                        <div className="line"></div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="bo1">
                        <div className={'t-box'}>
                            <img src={login1} alt="" />
                            <p style={{fontSize: "32px", margin: '32px 0 21px'}}>开启你的元宇宙之旅</p>
                            <p style={{
                                fontSize: "18px",
                                fontWeight: 'lighter'
                            }}>加入MetaEye社区，体验全新的虚拟世界</p>
                        </div>
                        <div className="line"></div>
                    </div>
                </Carousel.Item>
            </Carousel>
            <div className="content-info">
                <div className="info1">
                    <div className="name">社区</div>
                    <div className="acti">最新活动</div>
                    <div className="acti-info">
                        <div>
                            <div className="time">11月23日活动</div>
                            <div className="acti-name">沙漠绿洲“MetaEye”开启新纪元</div>
                            <div className="acti-tip">活动简介小短句，随便几十个字超了就...</div>
                        </div>
                        <div>
                        <div className="time">11月23日活动</div>
                            <div className="acti-name">沙漠绿洲“MetaEye”开启新纪元</div>
                            <div className="acti-tip">活动简介小短句，随便几十个字超了就...</div>
                        </div>
                    </div>
                    <div className="img-box-info">
                        <div className="img-box">
                            <div className="img"></div>
                            <div className="img-r">
                                <div className="img-r-1">信息来源</div>
                                <div className="img-r-2">神秘玛丽苏风暴沙漠探险日记93天</div>
                                <div className="img-r-3">发布日期显示</div>
                            </div>
                        </div>
                        <div className="img-box">
                            <div className="img"></div>
                            <div className="img-r">
                                <div className="img-r-1">信息来源</div>
                                <div className="img-r-2">神秘玛丽苏风暴沙漠探险日记93天</div>
                                <div className="img-r-3">发布日期显示</div>
                            </div>
                        </div>
                        <div className="img-box">
                            <div className="img"></div>
                            <div className="img-r">
                                <div className="img-r-1">信息来源</div>
                                <div className="img-r-2">神秘玛丽苏风暴沙漠探险日记93天</div>
                                <div className="img-r-3">发布日期显示</div>
                            </div>
                        </div>
                    </div>
                    <div className="info-left info-left-info">
                        <div className="info-left-1">
                            <div className="info-left-name">你的虚拟化身</div>
                            <div className="info-left-content">
                                <div>
                                虚拟化身是你进入MetaEye元宇宙的通行证，在这里
                                </div>
                                <div>
                                你将可以提前体验未来的生活方式和商业模式，开启
                                </div>
                                <div>
                                一段与众不同的元宇宙之旅。
                                </div>
                            </div>
                            <div className="info-left-btn">创造你的身份</div>
                        </div>
                        <div className="img1">

                        </div>
                    </div>
                    <div className="info-left">
                        <div className="img2"></div>
                        <div className="info-left-1">
                            <div className="info-left-name">你的虚拟化身</div>
                            <div className="info-left-content">
                                <div>
                                虚拟化身是你进入MetaEye元宇宙的通行证，在这里
                                </div>
                                <div>
                                你将可以提前体验未来的生活方式和商业模式，开启
                                </div>
                                <div>
                                一段与众不同的元宇宙之旅。
                                </div>
                            </div>
                            <div className="info-left-btn">创造你的身份</div>
                        </div>
                    </div>
                    <div className="info-left">
                        <div className="info-left-1">
                            <div className="info-left-name">你的虚拟化身</div>
                            <div className="info-left-content">
                                <div>
                                虚拟化身是你进入MetaEye元宇宙的通行证，在这里
                                </div>
                                <div>
                                你将可以提前体验未来的生活方式和商业模式，开启
                                </div>
                                <div>
                                一段与众不同的元宇宙之旅。
                                </div>
                            </div>
                            <div className="info-left-btn">创造你的身份</div>
                        </div>
                        <div className="img3">

                        </div>
                    </div>
                    <div className="info-left last-info">
                        <div className="img4"></div>
                        <div className="info-left-1">
                            <div className="info-left-name">你的虚拟化身</div>
                            <div className="info-left-content">
                                <div>
                                虚拟化身是你进入MetaEye元宇宙的通行证，在这里
                                </div>
                                <div>
                                你将可以提前体验未来的生活方式和商业模式，开启
                                </div>
                                <div>
                                一段与众不同的元宇宙之旅。
                                </div>
                            </div>
                            <div className="info-left-btn">创造你的身份</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
