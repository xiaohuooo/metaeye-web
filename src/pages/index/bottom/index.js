import React from "react";
import "./index.css";
import 'components/Styles.css'

import logo from 'assets/image/logo.png';

import logoR from 'assets/icons/R.png';
import logoFacebook from 'assets/icons/facebook-fill.png';
import logoYtb from 'assets/icons/youtube.png';
import logoTwitter from 'assets/icons/twitter.png';
import logoIns from 'assets/icons/instagram-fill.png';
import logoTele from 'assets/icons/telegram(1).png';
import logoDiscard from 'assets/icons/reddit-logo-16.png';

export default function index() {
    return (
        <div className={'vertical-flex foot-box'}
             style={{
                 paddingTop: "50px"
             }}>
            <img src={logo} alt={''}/>
            <div className={'horizontal-flex big-top-magin'} style={{
                width: "100%"
            }}>
                <div className={'vertical-flex'}>
                    {' '} 关注我们
                    <div className={'horizontal-flex default-top-magin'}>
                        <img src={logoR} className={'icon'} alt=""/>
                        <img src={logoFacebook} className={'icon'} alt=""/>
                        <img src={logoYtb} className={'icon'} alt=""/>
                        <img src={logoTwitter} className={'icon'} alt="" style={{marginRight: 0}}/>
                    </div>
                    <div className={'horizontal-flex'}>
                        <img src={logoIns} className={'icon'} alt=""/>
                        <img src={logoTele} className={'icon'} alt=""/>
                        <img src={logoDiscard} className={'icon'} alt=""/>
                    </div>

                </div>
                <div style={{width: "30%"}} />
                <div className={'vertical-flex l-other'}>
                    <div className={'horizontal-flex l-other-line'}>
                        <p>开始游戏</p>
                        <p>地图</p>
                        <p>市场</p>
                        <p>新闻</p>
                    </div>
                    <div className={'horizontal-flex l-other-line'}>
                        
                        <p>公司</p>
                        <p>伙伴</p>
                        <p>白皮书</p>
                        <p>帮助中心</p>
                    </div>

                    <div className={'horizontal-flex l-other-line'}>
                        <p>使用条款</p>
                        <p> 内容政策</p>
                        <p> 隐私政策</p>
                        <p> 道德规范</p>
                    </div>
                </div>
            </div>
        </div>
        // <div style={{ marginTop: "145px", width: "80%", margin: "auto" }}>
        //   <div className="logo1"></div>
        //   <div className="b-box">
        //     <div className="b-left" style={{marginLeft:'50px'}}>
        //       <p style={{ fontSize: "24px", marginBottom: "30px" }}>关注我们</p>
        //       <div className="b-img">
        //         <img src={logo} className={'icon'} alt="" />
        //         <img src={logo1} className={'icon'} alt="" />
        //         <img src={logo2} className={'icon'} alt="" />
        //         <img src={logo3} className={'icon'} alt="" style={{ marginRight: 0 }} />
        //         <br />
        //         <img src={logo4} className={'icon'} alt="" />
        //         <img src={logo5} className={'icon'} alt="" />
        //         <img src={logo6} className={'icon'} alt="" />
        //       </div>
        //     </div>
        //     <div className="b-right">
        //       <div>
        //         <p>开始游戏</p>
        //         <p>地图</p>
        //         <p>市场</p>
        //         <p>新闻</p>
        //       </div>
        //       <div>
        //         <p>公司</p>
        //         <p>伙伴</p>
        //         <p>白皮书</p>
        //         <p>帮助中心</p>
        //       </div>
        //       <div>
        //         <p>使用条款</p>
        //         <p> 内容政策</p>
        //         <p> 隐私政策</p>
        //         <p> 道德规范</p>
        //       </div>
        //     </div>
        //   </div>
        // </div>
    );
}
