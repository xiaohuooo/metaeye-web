import React from "react";
import Center from "pages/index/center";
import Bottom from "pages/index/bottom";
import 'pages/index/Index.css'

import message from "assets/image/message.png"
import yu from "assets/image/yu.png"
import tou from "assets/image/tou.png"

import person from "assets/image/person.png"
import activ from "assets/image/activ.png"
import address from "assets/image/address.png"
import good from "assets/image/good.png"
import setting from "assets/image/setting.png"
import ya from "assets/image/ya.png"
import kong from "assets/image/kong.png"
import fen from "assets/image/fen.png"
import mess from "assets/image/mess.png"
import hao from "assets/image/hao.png"
import add from "assets/image/add.png"

const headerStyle = {
    textAlign: "center",
    color: "#000",
    height: "64px",
    paddingInline: 50,
    lineHeight: "60px",
    backgroundColor: "#fff",
};

const footerStyle = {
    color: "#000",
    height: "360px",
    backgroundColor: "#fff",
};

export default function Index() {
    return (
        <div style={{
            overflowY: "auto",
            overflowX: "hidden",
            height: "100vh"
        }}>
            <div style={{
            }}>
                <div className="header" style={headerStyle}>
                    <div className="logo"></div>
                    <div className="header-r">
                        <div>
                            <img src={message} alt="" />
                            <div className="info">
                                <div>消息中心</div>
                                <div>有一则新的消息，请查看......</div>
                                <div>新的交易提醒，请注意查收......</div>
                                <div>有一则新的消息,请查看......</div>
                            </div>
                            <div className="point">29</div>
                        </div>
                        <div>
                            <img src={yu} alt="" />
                        </div>
                        <div className="tou">
                            <img src={tou} alt="" />
                        </div>
                        <div className="mete">进入MetaEye</div>
                    </div>
                </div>
                <div className="center-box">
                    <div className="center-box-left">
                        <div className="active">
                            <img src={person} alt="" />
                        </div>
                        <div>
                            <img src={activ} alt="" />
                        </div>
                        <div>
                            <img src={address} alt="" />
                        </div>
                        <div>
                            <img src={good} alt="" />
                        </div>
                        <div>
                            <img src={setting} alt="" />
                        </div>
                        <div>
                            <img src={ya} alt="" />
                        </div>
                        <div>
                            <img src={kong} alt="" />
                        </div>
                        <div>
                            <img src={fen} alt="" />
                        </div>
                        <div>
                            <img src={mess} alt="" />
                        </div>
                    </div>
                    <div className="center-box-info">
                        <Center/>
                    </div>
                    <div className="center-box-right">
                        <img src={hao} alt="" />
                        <div className="list-box">
                            <img src={tou} alt=""/>
                        </div>
                        <div className="list-box">
                            <img src={tou} alt=""/>
                        </div>
                        <div className="list-box">
                            <img src={tou} alt=""/>
                        </div>
                        <div className="list-box">
                            <img src={tou} alt=""/>
                        </div>
                        <div className="list-box">
                            <img src={tou} alt=""/>
                        </div>
                        <img src={add} alt="" />
                    </div>
                </div>
                <div style={footerStyle}>
                    <Bottom></Bottom>
                </div>
            </div>

        </div>
    );
}
