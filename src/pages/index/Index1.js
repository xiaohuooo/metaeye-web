import React from "react";
import Center from "pages/index/center";
import Bottom from "pages/index/bottom";
import 'pages/index/Index.css'

const headerStyle = {
    textAlign: "center",
    color: "#000",
    height: "60px",
    paddingInline: 50,
    lineHeight: "60px",
    backgroundColor: "#fff",
};

const footerStyle = {
    color: "#000",
    height: "360px",
    backgroundColor: "#f5f5f5",
};

export default function Index() {
    return (
        <div style={{
            overflowY: "auto",
            overflowX: "hidden",
            height: "100vh"
        }}>
            <div style={{
            }}>
                <div style={headerStyle}>
                    <div className="logo"></div>
                    <div style={{ float: "right", marginTop: "-25px",fontSize:'20px' }} />
                </div>
                <Center/>
                <div style={footerStyle}>
                    <Bottom></Bottom>
                </div>
            </div>

        </div>
    );
}
