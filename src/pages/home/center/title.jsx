import React from "react";
import logo1 from "assets/素材/向左.png";
export default function title(props) {
    const { tit1,tit2,tit3,mode } = props;
  return (
    <>
      <p style={{ fontSize: "28px", color: "red" }}>{tit1}</p>
      <p style={{ fontSize: "89px", marginTop: mode?"35px":"55px",color:'#000' }}>
        {tit2[0]}
        <br />
        {tit2[1]}
        <br />
        {tit2[2]||''}
      </p>
      <p style={{ fontSize: "28px", marginTop: mode?"40px":"55px", color: "#7d7d7d" }}>
        <img
          src={logo1}
          alt=""
          style={{
            transform: "rotateY(180deg)",
            width: "48px",
            verticalAlign: "middle",
          }}
        />
        <span
          style={{
            marginLeft: "10px",
          }}
        >
          {tit3}
        </span>
      </p>
    </>
  );
}
