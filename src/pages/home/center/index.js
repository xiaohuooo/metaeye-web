import "pages/index/center/index.css";
import logo from "assets/素材/指向-左.png";
import Title from "./title";
import Title1 from "./title1";
import React, {useState} from "react";
import logo1 from "assets/素材/向左.png";
import exampleImg01 from "assets/image/index/index-example-img01.png"
import exampleImg02 from "assets/image/index/index-example-img02.png"
import exampleImg03 from "assets/image/index/index-example-img03.png"

const tNav = {
    height: "65px",
    borderBottom: "1px solid gray",
    lineHight: "65px",
    background: "#fff",
    color: "#000",
    lineHeight: "65px",
    paddingLeft: "13%",
};

export default function Index() {
    const [activeIndex, setActiveIndex] = useState(0);
    const handleClick = (index) => {
        setActiveIndex(index);
    };

    return (
        <div>
            <div className="bo1">
                <div className={'t-box'}>
                    <p style={{fontSize: "72px"}}>欢迎来到MetaEye</p>
                    <p style={{
                        fontSize: "24px",
                        fontWeight: 'lighter',
                        marginTop: '10px'
                    }}>我们正在打造新一代互联网平行世界</p>
                    <div className="t-button">
                        <button className={'bo1-button01 text-normal'}>探索MetaEye -></button>
                        <button className={'bo1-button02 text-normal'}>游客访问</button>
                    </div>
                </div>
            </div>
            <div style={tNav} className="t-nav">
                <span
                    onClick={() => handleClick(0)}
                    className={activeIndex === 0 ? "t-dr" : "t-dr-default"}
                >
                  什么是MetaEye?
                </span>
                <span
                    onClick={() => handleClick(1)}
                    className={activeIndex === 1 ? "t-dr" : "t-dr-default"}
                >
                  加入我们
                </span>
                <span
                    onClick={() => handleClick(2)}
                    className={activeIndex === 2 ? "t-dr" : "t-dr-default"}
                >
                  品牌社区
                </span>
                <span
                    onClick={() => handleClick(3)}
                    className={activeIndex === 3 ? "t-dr" : "t-dr-default"}
                >
                  安全与技术
                </span>
            </div>

            <div className={'tb-content'}>
                {activeIndex === 0 ? (
                    <div className="n-box">
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "38px"}}>
                                让我们一起打破现实和虚拟世界之间的界限，MetaEye正在
                                <br/>
                                推动下一代社交技术和商业形态的发展。
                            </p>
                            <p style={{fontSize: "24px", marginTop: "45px"}}>
                                我们将不再受限于2D屏幕，轻松体验3D可视化和虚拟现实的全新感受，帮助
                                <br/>
                                人们建立更紧密的社交联系和更高效的业务网络。
                            </p>
                        </div>
                    </div>
                ) : activeIndex === 1 ? (
                    <div className="n-box">
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "38px"}}>
                                111111111111
                                <br/>
                                2222222
                            </p>
                            <p style={{fontSize: "24px", marginTop: "45px"}}>
                                11111111111
                                <br/>
                                22222222
                            </p>
                        </div>
                    </div>
                ) : activeIndex === 2 ? (
                    <div className="n-box">
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "38px"}}>
                                22222
                                <br/>
                                33333
                            </p>
                            <p style={{fontSize: "24px", marginTop: "45px"}}>
                                222222222
                                <br/>
                                3333333333
                            </p>
                        </div>
                    </div>
                ) : (
                    <div className="n-box">
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "38px"}}>
                                33333333333
                                <br/>
                                44444444
                            </p>
                            <p style={{fontSize: "24px", marginTop: "45px"}}>
                                333333333
                                <br/>
                                444444444
                            </p>
                        </div>
                    </div>
                )}
            </div>

            <div className="n-box1">
                <div style={{textAlign: "center", paddingTop: "115px"}}>
                    <Title
                        tit1="创造力"
                        tit2={["MetaEye将塑造我们在", "元宇宙中的专属空间"]}
                        tit3="加入我们"
                        mode={false}
                    ></Title>
                    <img
                        src={exampleImg01}
                        style={{
                            width: "70%",
                            marginTop: "100px"
                        }}
                        alt={''}/>
                    <p
                        style={{
                            fontSize: "32px",
                            color: "#000",
                            textAlign: "left",
                            width: "70%",
                            margin: "auto",
                            paddingTop: "100px"
                        }}
                    >
                        <img
                            src={logo}
                            alt=""
                            style={{
                                transform: "rotateZ(-90deg)",
                                width: "28px",
                                verticalAlign: "middle",
                            }}
                        />
                        <span
                            style={{
                                marginLeft: "30px",
                            }}
                        >
              最新的活动
            </span>
                    </p>
                    <div className="horizontal-flex center-layout"
                         style={{
                             paddingBottom: "150px",
                         }}>
                        <div className={'event-box'}>
                            属于您的专属空间
                        </div>
                        <div style={{width: "6%"}}/>
                        <div className={'event-box'}>
                            属于您的专属空间
                        </div>
                    </div>
                </div>
            </div>

            <div className="n-box2">
                <div style={{textAlign: "center", paddingTop: "115px"}}>
                    <Title
                        tit1="在这里"
                        tit2={["每个人都能轻松的构建", "自己的元宇宙空间"]}
                        tit3="了解更多"
                        mode={true}
                    ></Title>
                    <img
                        src={exampleImg02}
                        style={{
                            width: "100%",
                            marginTop: "100px",
                            marginBottom: "100px"
                        }}
                        alt={''}/>
                    <Title
                        tit1="品牌营销"
                        tit2={[
                            "超过 200 万商户使用 MetaEye 的",
                            " 元宇宙营销场景——其中大多数",
                            "是中小型企业。",
                        ]}
                        tit3="了解更多"
                        mode={true}
                    ></Title>
                    <hr style={{width: "80%", margin: "100px auto"}}/>
                    <Title
                        tit1="技术"
                        tit2={["让数字成为你的资产", "让创造变得轻松"]}
                        tit3="了解更多"
                        mode={true}
                    ></Title>

                    <div className="horizontal-flex center-layout"
                         style={{
                             marginTop: "100px"
                         }}>
                        <div
                            style={{
                                width: "38%",
                                height: "600px",
                                backgroundColor: "black",
                                borderWidth: "3px",
                                borderColor: "#fb2d55",
                                borderStyle: "solid",
                                color: "azure",
                            }}>
                            <div
                                style={{
                                    paddingLeft: "15%",
                                    paddingRight: "15%",
                                    textAlign: "left"
                                }}>
                                <div style={{fontSize: "42px", marginTop: "80px"}}>
                                    <p>
                                        资产与安全
                                    </p>
                                </div>
                                <div style={{fontSize: "24px", marginTop: "40px"}}>
                                    <p>
                                        我们致力于提供基于区块链技术的隐 私保护和安全性，
                                        让您的资产得到最好的保护。同时，我们也支持您对数
                                        据的自主控制和使用，让您拥有安全的数据主权。
                                    </p>
                                </div>
                                <div className={'horizontal-flex'}
                                     style={{
                                         justifyContent: "start",
                                         marginTop: "60px"
                                     }}>
                                    <img
                                        src={logo1}
                                        alt=""
                                        style={{
                                            transform: "rotateY(180deg)",
                                            width: "48px",
                                            marginRight: "20px",
                                            verticalAlign: "middle",
                                        }}
                                    />
                                    了解更多
                                </div>
                            </div>
                        </div>
                        <div className={'n-image-box'}/>
                    </div>
                </div>
            </div>
            <hr style={{width: "80%", margin: "100px auto"}}/>
            <div className="n-box3">
                <p
                    style={{
                        fontSize: "32px",
                        color: "#000",
                        textAlign: "left",
                        width: "70%",
                        margin: " 0 auto 50px",
                    }}
                >
                    <img
                        src={logo}
                        alt=""
                        style={{
                            transform: "rotateZ(-90deg)",
                            width: "28px",
                            verticalAlign: "middle",
                        }}
                    />
                    <span
                        style={{
                            marginLeft: "30px",
                        }}
                    > 新闻与文字 </span>
                </p>
            </div>

            <div className="n-news-container horizontal-flex">
                <div className="box">
                    <div className="b-img"></div>
                    <Title1
                        tit1="此处小标题"
                        tit2={["此处输入大标题", " 大标题大标题大标题"]}
                        tit3="这里是简短的新闻介绍，白日 依山尽，黄河入海流。欲穷千 里目，更上一层楼。"
                        tit4="了解更多"
                    ></Title1>
                </div>
                <div style={{width: "5%"}}/>
                <div className="box">
                    <div className="b-img"></div>
                    <Title1
                        tit1="此处小标题"
                        tit2={["此处输入大标题", " 大标题大标题大标题"]}
                        tit3="这里是简短的新闻介绍，白日 依山尽，黄河入海流。欲穷千 里目，更上一层楼。"
                        tit4="了解更多"
                    ></Title1>
                </div>
                <div style={{width: "5%"}}/>
                <div className="box">
                    <div className="b-img"></div>
                    <Title1
                        tit1="此处小标题"
                        tit2={["此处输入大标题", " 大标题大标题大标题"]}
                        tit3="这里是简短的新闻介绍，白日 依山尽，黄河入海流。欲穷千 里目，更上一层楼。"
                        tit4="了解更多"
                    ></Title1>
                </div>
            </div>

            <hr style={{width: "80%", margin: "100px auto"}}/>
            <div className="horizontal-flex center-layout"
                 style={{
                     marginLeft: "20%",
                     marginRight: "20%",
                     justifySelf: "center",
                     alignItems: "start"
                 }}>
                <div className={'n-contact-box vertical-flex'}>
                    <p style={{
                        fontSize: "36px",
                        color: "black",
                        marginBottom: "20px"
                        }}>
                        联系我们
                    </p>
                    <p
                        style={{
                            fontSize: "18px",
                            marginBottom: "30px",
                            color: "black",
                            height: "150px"
                        }}
                    >
                        您可以随时在 Discord 上找到我们。您也可以通过常规 渠道
                        <br/>
                        联系我们。
                    </p>
                    <div
                        style={{
                            width: "175px",
                            height: "38px",
                            background: "red",
                            lineHeight: "38px",
                            textAlign: "center",
                        }}
                    >
                        保持联系
                    </div>
                </div>

                <div style={{width: "15%"}}/>

                <div className={'n-contact-box vertical-flex'}>
                    <p style={{
                        fontSize: "36px",
                        color: "black",
                        marginBottom: "20px"
                    }}>
                        获取最新更新
                    </p>
                    <p
                        style={{
                            fontSize: "18px",
                            marginBottom: "30px",
                            color: "black",
                            height: "150px"
                        }}
                    >
                        有关 MetaEye 活动、竞赛和新发展的突发新闻，直接发
                        <br/>
                        送到您的收件箱
                    </p>

                    <div className={'horizontal-flex'}>
                        <div
                            style={{
                                width: "320px",
                                height: "38px",
                                backgroundColor: "gray",
                                lineHeight: "38px",
                                textAlign: "left",
                                paddingLeft: "20px",
                                marginRight: "20px",
                                display: "inline-block",
                            }}
                        >
                            邮箱@MetaEye.com
                        </div>
                        <div
                            style={{
                                width: "120px",
                                height: "38px",
                                background: "red",
                                lineHeight: "38px",
                                textAlign: "center",
                                display: "inline-block",
                            }}
                        >
                            报名
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
