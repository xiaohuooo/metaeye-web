import './logo.css'
import React, {useState} from "react";
import axios from "axios";
import Const from "utils/Const";
// import {isMobile } from 'react-device-detect';

import DefaultNavBar from "components/navbars/DefaultNavBar";
import NormalButton from "components/buttons/NormalButton";
import CheckBox from "components/check-box/CheckBox";
import {useNavigate} from "react-router-dom";

import FullscreenLoader from "components/FullscreenLoader";
import NotifyWindow from "components/notify-window/NotifiyWindow";
import UserService from "services/UserService";

import login1 from 'assets/image/login1.png'
import wallet from 'assets/image/wallet.png'
import eye from 'assets/image/eye.png'

export default function Login(Props) {
    let navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [psd, setPsd] = useState('');
    const [loading, setLoading] = useState(false);
    const [notifying, setNotifying] = useState({
        title : 'notify',
        content : 'content',
        display: false
    });

    function onConfirmNotify() {
        setNotifying({
            display: false
        });
    }

    async function login() {
        if(!UserService.isValidEmail(email)) {
            setNotifying({
                title: 'Ops',
                content: 'Please enter a valid email.',
                display: true
            });
            return;
        }

        if(psd.length < 8) {
            setNotifying({
                title: 'Ops',
                content: 'Please enter a valid password.',
                display: true
            });
            return;
        }

        const rootUrl = Const.serverHttpRoot;
        const requestBody = {
            type: '',
            body: {
                email: email,
                psd: psd
            }
        };
        setLoading(true);

        try {
            const response = await axios.put(rootUrl + 'user/login', requestBody)

            setLoading(false);
            const res = response.data;
            if(res.success > 0) {
                UserService.loginUser(email, res.attachment.name);
                navigate('/');
                return;
            }

            setNotifying({
                title: 'Failed',
                content: 'Server return: ' + res.error,
                display: true
            });
        } catch (e) {
            setLoading(false);
            setNotifying({
                title: 'Failed',
                content: e.toString(),
                display: true
            });
        }
    }

    function register() {
        navigate('/user/register');
    }


    return (
        <div className={'overlay-fullscreen user-reception-backgound autoflow-vertical'}>
            <img className="login" src={login1} alt="" />
            <div className='title'>进入元宇宙</div>
            <div className={'user-reception-frame'}>
                <div className={'user-reception-content-container'}>
                    <div className={'user-reception-form-item'}>
                        <div className='name'>用户名或电子邮件</div>
                        <input type={'email'} onInput={(e) => setEmail(e.target.value)}
                            className={' user-reception-input text-normal'}></input>
                    </div>
                    <div className={'pwd user-reception-form-item text-title-2'}>
                        <div className='name'>密码</div>
                        <input type={'password'} onInput={(e) => setPsd(e.target.value)}
                            className={' user-reception-input text-normal'}></input>
                        <img className='eye' src={eye} alt="" />
                    </div>

                    <div className={''}/>
                    <div className={'Remember horizontal-flex'}>
                        忘记密码？
                    </div>
                    <div className='btn'>
                        登录
                    </div>
                    <div className='more'>或者</div>
                    {/* <NormalButton onClick={login} content={'Login'} important={true}></NormalButton> */}
                    <div className='wallet-btn'>
                        <img src={wallet} alt=""/>
                        <div>连接钱包</div>
                    </div>
                    <div className='ifno'>
                        <div>没有账户？</div>
                        <div onClick={register}>创建一个 > </div>
                    </div>
                    {/* <NormalButton onClick={register} content={'Register'}></NormalButton> */}
                </div>
            </div>
            <div/>
            <NotifyWindow title={notifying.title} show={notifying.display} content={notifying.content} onConfirm={onConfirmNotify}/>
            <FullscreenLoader show={loading}></FullscreenLoader>
        </div>
    )
}