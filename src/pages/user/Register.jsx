import './UserReception.css'
import DefaultNavBar from "components/navbars/DefaultNavBar";
import React, {useState} from "react";
import NormalButton from "components/buttons/NormalButton";
import {useNavigate} from "react-router-dom";
import FullscreenLoader from "components/FullscreenLoader";
import NotifyWindow from "components/notify-window/NotifiyWindow";
import Const from "utils/Const";
import axios from "axios";
import UserService from "services/UserService";
import {isMobile} from "react-device-detect";

import login1 from 'assets/image/login1.png'
import eye from 'assets/image/eye.png'

export default function Register(Props) {

    let navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [psd, setPsd] = useState('');
    const [psdConfirm, setPsdConfirm] = useState('');
    const [loading, setLoading] = useState(false);
    const [notifying, setNotifying] = useState({
        title : 'notify',
        content : 'content',
        display: false
    });

    function onConfirmNotify() {
        setNotifying({
            display: false
        });
    }

    async function register() {
        if(!UserService.isValidEmail(email)) {
            setNotifying({
                title: 'Ops',
                content: 'Please enter a valid email.',
                display: true
            });
            return;
        }

        if(name === '') {
            setNotifying({
                title: 'Ops',
                content: 'Please enter a name.',
                display: true
            });
            return;
        }

        if(psd.length < 8) {
            setNotifying({
                title: 'Ops',
                content: 'Password length should greater than 8!',
                display: true
            });
            return;
        }

        if(psd !== psdConfirm) {
            setNotifying({
                title: 'Ops',
                content: 'Password and Password Confirm should be the same.',
                display: true
            });
            return;
        }

        const rootUrl = Const.serverHttpRoot;
        const requestBody = {
            type: '',
            body: {
                email: email,
                name: name,
                psd: psd
            }
        };

        try {
            setLoading(true);
            const response = await axios.put(rootUrl + 'user/register', requestBody)
            setLoading(false);

            const res = response.data;
            if(res.success > 0) {
                UserService.loginUser(email, name, psd);
                navigate('/');
                return;
            }

            setNotifying({
                title: 'Failed',
                content: 'Server return: ' + res.error,
                display: true
            });
        } catch (e) {
            setLoading(false);
            setNotifying({
                title: 'Failed',
                content: e.toString(),
                display: true
            });
        }
    }

    function back() {
        navigate('/user/continue');
    }

    return (
        <div className={'overlay-fullscreen user-reception-backgound autoflow-vertical'}>
            <img className="login" src={login1} alt="" />
            <div className='title'>注册账户</div>
            <div className={'user-reception-frame'}>
                <div className={'user-reception-content-container'}>
                    <div className={'user-reception-form-item'}>
                        <div className='name'>用户名</div>
                        <input type={'email'} onInput={(e) => setEmail(e.target.value)}
                            className={' user-reception-input text-normal'}></input>
                    </div>
                    <div className={'pwd user-reception-form-item'}>
                        <div className='name'>电子邮箱</div>
                        <input type={'email'} onInput={(e) => setEmail(e.target.value)}
                            className={' user-reception-input text-normal'}></input>
                    </div>
                    <div className={'pwd user-reception-form-item text-title-2'}>
                        <div className='name'>密码</div>
                        <input type={'password'} onInput={(e) => setPsd(e.target.value)}
                            className={' user-reception-input text-normal'}></input>
                        <img className='eye' src={eye} alt="" />
                    </div>

                    <div className={''}/>
                    <div className={'tip horizontal-flex'}>
                        使用8个或更多字符，混合使用字母、数字和符合。
                    </div>
                    <div className='btn' onClick={back}>
                        注册
                    </div>
                    <div className='agreement'>
                        <input type="radio" name="bb" id="woman"/>
                        <div>创建账户即表示您同意我们的</div>
                        <div className='agree-info'>使用条款</div>
                    </div>
                </div>
            </div>
            <div/>
            <NotifyWindow title={notifying.title} show={notifying.display} content={notifying.content} onConfirm={onConfirmNotify}/>
            <FullscreenLoader show={loading}></FullscreenLoader>
        </div>
    )
}